package com.seanyung.aimltranslator;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.seanyung.aimltranslator.exception.RemoteServerException;
import com.seanyung.aimltranslator.util.Log;
import com.seanyung.aimltranslator.util.TransUtils;

public class Main {
	private static int fileCount = 0;
	private static int finishedCount = 0;
	private static int failedCount = 0;
	private static String srcPath;
	private static String tarPath;
	private static boolean translate;
	private static boolean segment;

	/**
	 * @param args
	 * @throws IOException
	 * @throws RemoteServerException
	 */
	public static void main(String[] args) throws IOException, RemoteServerException {

		if (args == null || args.length < 2
				||args.length >4) {
			Log.log("\n正确格式：\n" +
					"    java - jar transXml.jar sourceFolder targetFolder[ -translate][ -segment],\n" +
					"         sourceFolder：要处理的原文件目录\n" +
					"         targetFolder：处理完成文件的输出目录\n" +
					"         -translate：可选，对目录内文件进行英中处理\n" +
					"         -segment：可选，对目录内文件进行分词操作\n" +
					"如果两个可选参数均未填写，默认进行处理操作\n");
			return;
		}
		if(args.length==2){
			translate=true;
			segment=false;
		}else if (args.length==3) {
			if("-translate".equals(args[2])){
				translate=true;
			}else if("-segment".equals(args[2])){
				segment=true;
			}else{
				Log.log("参数三 格式不正确：" +
						"         -translate：可选，对目录内文件进行英中处理\n" +
						"         -segment：可选，对目录内文件进行分词操作\n" +
						"如果两个可选参数均未填写，默认进行处理操作");
			}
			
		}else if (args.length==4) {
			if("-segment".equals(args[3])||"-translate".equals(args[2])){
				translate=true;
				segment=true;
			}else if("-segment".equals(args[2])||"-segment".equals(args[3])){
				segment=true;
				segment=true;
			}else{
				Log.log("参数三或者四 格式不正确：" +
						"         -translate：可选，对目录内文件进行英中处理\n" +
						"         -segment：可选，对目录内文件进行分词操作\n" +
						"如果两个可选参数均未填写，默认进行处理操作");
			}
		}

		String arg0 = args[0].replace("\\", File.separator).replace("/",
				File.separator);
		String arg1 = args[1].replace("\\", File.separator).replace("/",
				File.separator);
		if (!arg0.endsWith(File.separator)) {
			arg0 = arg0 + File.separator;
		}
		if (!arg1.endsWith(File.separator)) {
			arg1 = arg1 + File.separator;
		}
		srcPath = arg0;
		tarPath = arg1;

		Log.info("\n\n\n");
		File file = new File(srcPath);
		Log.info(new SimpleDateFormat("yyyy-M-d h:m:s").format(new Date()));
		final StringBuilder stringBuilder=new StringBuilder("已完成文件：");
		File[] files = file.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				fileCount++;
				if (new File(tarPath + pathname.getName()).exists()) {
					stringBuilder.append(pathname.getName() +"、");
					finishedCount++;
					return false;
				}
				return true;
			}
		});
		if("已完成文件：".length()+1<stringBuilder.length()){
			Log.info(stringBuilder.toString());
		}

		for (int i = 0; i < files.length; i++) {
			Log.info("-------------------"+ files[i].getName()+"----------------------");
			Log.info("总计需处理文件" + fileCount + "个,已处理" + finishedCount + "个，失败" + failedCount + "个");
			Log.info("正在处理第" + (finishedCount + failedCount + 1) + "个文件（"
					+ files[i].getName() + "）,剩余"
					+ (fileCount - finishedCount - failedCount - 1) + "个：");
			Log.info("…………");
			try {
				TransUtils.transAiml(tarPath,files[i],translate,segment);
				Log.info("第" + (finishedCount + failedCount + 1) + "个文件（"
						+ files[i].getName() + "）处理完毕！");
				finishedCount++;
			} catch (RemoteServerException transe) {
				transe.printStackTrace();
				Log.error("第" + (finishedCount + failedCount + 1) + "个文件（"
						+ files[i].getName() + "）处理失败！");
				Log.error("错误信息：", transe);
				throw transe;
			} catch (Exception e) {
				e.printStackTrace();
				Log.error("第" + (finishedCount + failedCount + 1) + "个文件（"
						+ files[i].getName() + "）处理失败！");
				Log.error("错误信息：", e);
				failedCount++;
			}
			
			Log.info("-------------------------------------------------------");
		}
		Log.info("总体处理完成，共成功转换文件" + finishedCount + "个，失败" + failedCount + "个");
	}

	
}
