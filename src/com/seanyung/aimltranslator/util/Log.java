package com.seanyung.aimltranslator.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Log {


	private static  int fileLevel=1;
	private static  int consoleLevel=0;
	private static PrintWriter log;
	static{
		try {
			log=new PrintWriter(new FileWriter("log.txt",true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setLevel(int level) {
		Log.fileLevel = level;
	}

	public static void error(String str,Exception e){
		if(fileLevel<4){
			log.println(str+":");
			for(StackTraceElement st:e.getStackTrace()){
				log.println(st.toString());
			}
			log.flush();
		}
		if(consoleLevel<4){
			System.out .println(str+":");
			for(StackTraceElement st:e.getStackTrace()){
				System.out.println(st.toString());
			}
		}
		
	}
	
	public static void error(String str){
		if(fileLevel<4){
			log.println(str);
			log.flush();
		}
		if(consoleLevel<4){
			System.out.println(str);
		}
	}
	
	public static void log(String str){
		if(fileLevel<1){
			log.println(str);
			log.flush();
			
		}
		if(consoleLevel<1){
			System.out.println(str);
		}
	}
	
	public static void warn(String str){
		if(fileLevel<3){
			log.println(str);
			log.flush();
		}
		if(consoleLevel<3){
			System.out.println(str);
		}
	} 

	public static void info(String str){
		if(fileLevel<2){
			log.println(str);
			log.flush();
		}
		if(consoleLevel<2){
			System.out.println(str);
		}
	}
}
