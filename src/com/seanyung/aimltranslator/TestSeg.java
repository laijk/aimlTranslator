package com.seanyung.aimltranslator;

import java.util.ArrayList;
import java.util.Scanner;

import org.wltea.analyzer.dic.Dictionary;

import com.seanyung.aimltranslator.util.TransUtils;

public class TestSeg {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		while(true){
			System.out.println("请输入：");
			String inputString=scanner.nextLine();
			if(inputString.startsWith("add")){
				String addedString=inputString.substring(3);
				ArrayList<String> arrList=new ArrayList<String>();
				arrList.add(addedString);
				Dictionary.getSingleton().addWords(arrList);
			}else if(inputString.equals("exit")){
				break;
			}else{
				System.out.println(TransUtils.cnSegment(inputString," "));
			}
		}
		
		scanner.close();
	}

}
